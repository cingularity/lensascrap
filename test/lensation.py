from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup


class Lensation(object):
    def __init__(self, link):
        self.link = link
        
        try:
            html = urlopen(self.link)
        except HTTPError as e:
            print("error", e)
            return None
        
        try:
            lenObj = BeautifulSoup(html.read(), 'html.parser')
        except AttributeError as e:
            print('error', e)
            return None
        
        self.lenObj = lenObj
            
        
    def getLinkSearchPage(self):
        link_details = self.lenObj.find_all('ul', {'class': 'page-numbers'})
        return link_details